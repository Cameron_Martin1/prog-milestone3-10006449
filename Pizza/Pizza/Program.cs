﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Milestone3_10006449
{
	class Program
	{
		static void Main(string[] args)
		{
			int launch;
			int i;
			int a;
			string pizzaflavour;



			List<Tuple<string, double>> pizza = new List<Tuple<string, double>>();
			pizza.Add(Tuple.Create("Small            ", 6.00));
			pizza.Add(Tuple.Create("Regular          ", 8.00));
			pizza.Add(Tuple.Create("Large            ", 15.00));
			pizza.Add(Tuple.Create("Jumbo            ", 20.00));

			List<Tuple<string, double>> drinks = new List<Tuple<string, double>>();
			drinks.Add(Tuple.Create(" 7 Up           ", 4.00));
			drinks.Add(Tuple.Create(" Pepsi          ", 4.00));
			drinks.Add(Tuple.Create(" Mountain Dew   ", 4.00));
			drinks.Add(Tuple.Create(" Rasperry       ", 4.00));
			drinks.Add(Tuple.Create(" Lemon Lime     ", 3.00));
			drinks.Add(Tuple.Create(" V              ", 3.00));

			List<Tuple<string, double>> sides = new List<Tuple<string, double>>();
			sides.Add(Tuple.Create("Lava Cake        ", 3.00));
			sides.Add(Tuple.Create("Donuts x5        ", 4.00));
			sides.Add(Tuple.Create("Caramel Moose    ", 4.50));

			Console.ForegroundColor = ConsoleColor.Green;
			Console.BackgroundColor = ConsoleColor.DarkGray;

			var type = new List<string> { "Simply Garlic      ", "Veganism           ", "Meatorgy           ", "Seafood            ", "Chicken & Cranberry", "Hawaiian            " };

		run_menu:
			Console.Clear();


			Console.WriteLine("***********************************************************");
			Console.WriteLine("***********************************************************");
			Console.WriteLine("**  Hello, welcome to Pizza Lab!                         **");
			Console.WriteLine("**  Do you wish to enter your details now, or later?     **");
			Console.WriteLine("**       1. Now                                          **");
			Console.WriteLine("**       2. Later                                        **");
			Console.WriteLine("***********************************************************");
			Console.WriteLine("***********************************************************");
			if (int.TryParse(Console.ReadLine(), out launch))
			{
				switch (launch)
				{
					case 1:
						cinput.getdetails();

						break;

					case 2:
						goto run_orderentry;

					default:
						{
							Console.WriteLine("***************************************************************");
							Console.WriteLine("*   Expression entered was deemed invalid - please try again. *");
							Console.WriteLine("***************************************************************");
							Console.ReadKey();
							goto run_menu;
						}

				}
			}
			else
			{
				Console.WriteLine("***************************************************************");
				Console.WriteLine("*   Expression entered was deemed invalid - please try again. *");
				Console.WriteLine("***************************************************************");
				Console.ReadKey();
				goto run_menu;
			}
		run_orderentry:


			Console.Clear();
			Order.ShowOrder();
			Console.WriteLine("************************************");
			Console.WriteLine("*      Would you like to do?       *");
			Console.WriteLine("*                                  *");
			Console.WriteLine("*   1. Order Pizza(s)              *");
			Console.WriteLine("*   2. Order Drink(s)              *");
			Console.WriteLine("*   3. Order Side (s)              *");
			Console.WriteLine("*   4. Finish & Pay                *");
			Console.WriteLine("*                                  *");
			Console.WriteLine("************************************");
			Console.WriteLine("*'Pizza Lab- making dreams reality'*");
			Console.WriteLine("************************************");
			if (int.TryParse(Console.ReadLine(), out a))
			{
				switch (a)
				{
					case 1:
					run_pizza:
						Console.Clear();

						Order.ShowOrder();
						for (i = 0; i < type.Count; i++)
						{
							Console.WriteLine($"{i + 1}. {type[i]}");
						}
						Console.WriteLine($"{i + 1}. Back to Main Menu");
						if (int.TryParse(Console.ReadLine(), out a))
						{
							if (a == i + 1)
							{
								goto run_orderentry;
							}
							else if (a > i + 1)
							{
								Console.WriteLine("***************************************************************");
								Console.WriteLine("*   Expression entered was deemed invalid - please try again. *");
								Console.WriteLine("***************************************************************");
								Console.ReadKey();
								goto run_pizza;
							}
							else
							{
								pizzaflavour = type[a - 1];
								Console.Clear();
								Order.ShowOrder();
								Console.WriteLine($"Your Selected Pizza: {pizzaflavour}");
								for (i = 0; i < pizza.Count; i++)
								{
									Console.WriteLine($"{i + 1}. {pizza[i].Item1}");
								}
								Console.WriteLine($"{i + 1}. Back to Main Menu");
								if (int.TryParse(Console.ReadLine(), out a))
								{
									if (a == i + 1)
									{
										goto run_orderentry;
									}
									else if (a > i + 1)
									{
										Console.WriteLine("***************************************************************");
										Console.WriteLine("*   Expression entered was deemed invalid - please try again. *");
										Console.WriteLine("***************************************************************");
										Console.ReadKey();
										goto run_pizza;
									}
									else
									{
										Order.order.Add(Tuple.Create(pizzaflavour, pizza[a - 1].Item1, pizza[a - 1].Item2));
									}
									goto run_pizza;
								}
								else
								{
									Console.WriteLine("***************************************************************");
									Console.WriteLine("*   Expression entered was deemed invalid - please try again. *");
									Console.WriteLine("***************************************************************");
									Console.ReadKey();
									goto run_pizza;
								}
							}
						}
						else
						{
							Console.WriteLine("***************************************************************");
							Console.WriteLine("*   Expression entered was deemed invalid - please try again. *");
							Console.WriteLine("***************************************************************");
							Console.ReadKey();
							goto run_pizza;
						}


					case 2:
					run_drink:

						Console.Clear();

						Order.ShowOrder();
						for (i = 0; i < drinks.Count; i++)
						{
							Console.WriteLine($"{i + 1}. {drinks[i].Item1}");
						}
						Console.WriteLine($"{i + 1}. Back to Main Menu");
						if (int.TryParse(Console.ReadLine(), out a))
						{
							if (a == i + 1)
							{
								goto run_orderentry;
							}
							else if (a > i + 1)
							{
								Console.WriteLine("***************************************************************");
								Console.WriteLine("*   Expression entered was deemed invalid - please try again. *");
								Console.WriteLine("***************************************************************");
								Console.ReadKey();
								goto run_drink;
							}
							else
							{
								Order.order.Add(Tuple.Create("Drink     ", drinks[a - 1].Item1, drinks[a - 1].Item2));
							}
							goto run_drink;
						}
						else
						{
							Console.WriteLine("***************************************************************");
							Console.WriteLine("*   Expression entered was deemed invalid - please try again. *");
							Console.WriteLine("***************************************************************");
							Console.ReadKey();
							goto run_drink;
						}

					case 3:
					run_sides:

						Console.Clear();

						Order.ShowOrder();
						for (i = 0; i < sides.Count; i++)
						{
							Console.WriteLine($"{i + 1}. {sides[i].Item1}");
						}
						Console.WriteLine($"{i + 1}. Back to Main Menu");
						if (int.TryParse(Console.ReadLine(), out a))
						{
							if (a == i + 1)
							{
								goto run_orderentry;
							}
							else if (a > i + 1)
							{
								Console.WriteLine("***************************************************************");
								Console.WriteLine("*   Expression entered was deemed invalid - please try again. *");
								Console.WriteLine("***************************************************************");
								Console.ReadKey();
								goto run_sides;
							}
							else
							{
								Order.order.Add(Tuple.Create("Desert     ", sides[a - 1].Item1, sides[a - 1].Item2));
							}
							goto run_sides;
						}
						else
						{
							Console.WriteLine("***************************************************************");
							Console.WriteLine("*   Expression entered was deemed invalid - please try again. *");
							Console.WriteLine("***************************************************************");
							Console.ReadKey();
							goto run_sides;
						}


					case 4:
					finish:
						if (cinput.phone == 0)
						{
							cinput.getdetails();
							goto finish;
						}
						else
						{

							Console.Clear();
							Order.ShowOrder();
							Console.WriteLine("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!                                   ");
							Console.WriteLine("ERROR 404");
							Console.WriteLine($"Hello, {cinput.name} unfortunately your order payment could not be fullfilled.           ");
							Console.WriteLine($"Your details including number ({cinput.displayphone}) have been registered in the system.");
							Console.WriteLine("Please proceed to a till to continue with payment                                         ");
							Console.WriteLine("          1. Proceed to pay at till                                                       ");
							Console.WriteLine("          2. Modify your order                                                            ");
							Console.WriteLine("                                                                                          ");
							Console.WriteLine("Thank you for using this ordering system, enjoy your day!                                 ");
							Console.WriteLine("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!                                   ");
							if (int.TryParse(Console.ReadLine(), out a))
							{
								switch (a)
								{
									case 1:
										Environment.Exit(0);
										break;

									case 2:
										goto run_orderentry;
								}
							}




						}
						break;

					default:
						Console.WriteLine("***************************************************************");
						Console.WriteLine("*   Expression entered was deemed invalid - please try again. *");
						Console.WriteLine("***************************************************************");
						Console.Read();
						goto run_orderentry;




				}
			}
			else
			{
				Console.WriteLine("***************************************************************");
				Console.WriteLine("*   Expression entered was deemed invalid - please try again. *");
				Console.WriteLine("***************************************************************");
				Console.Read();
				goto run_orderentry;
			}

		}
	}

	public static class Order
	{

		public static List<Tuple<string, string, double>> order = new List<Tuple<string, string, double>>();

		public static void ShowOrder()
		{
			int i;
			double total;
			string cash;
			total = 0;
			for (i = 0; i < order.Count; i++)
			{
				cash = String.Format("{0:C}", order[i].Item3);
				Console.WriteLine($"{order[i].Item1}      {order[i].Item2}      {cash}");
				total = total + order[i].Item3;
			}
			cash = String.Format("{0:C}", total);
			Console.WriteLine($"Order Total: {cash}");
		}

	}


	public static class cinput
	{
		public static string name;
		public static int phone;
		public static string displayphone;


		public static void getdetails()
		{
			int x;
		Start:
			Console.WriteLine("Please Enter a Name Below:");
			name = Console.ReadLine();

			Console.WriteLine("Please Enter a Phone Number Below:");
			if (int.TryParse(Console.ReadLine(), out phone))
			{
				displayphone = ($"0{phone}");
				Console.WriteLine($"Name: {name}");
				Console.WriteLine($"Number: {displayphone}");
				Console.WriteLine("");
				Console.WriteLine("If the information provided is accurate press (1) to proceed or (2) to go back.");
				Console.WriteLine("1. Proceed");
				Console.WriteLine("2. Go Back");

				if (int.TryParse(Console.ReadLine(), out x))
				{
					switch (x)
					{
						case 1:
							break;

						default:
							goto Start;
					}
				}
				else
				{
					Console.WriteLine("***************************************************************");
					Console.WriteLine("*   Expression entered was deemed invalid - please try again. *");
					Console.WriteLine("***************************************************************");
					Console.ReadKey();
					goto Start;
				}

			}
			else
			{
				Console.WriteLine("***************************************************************");
				Console.WriteLine("*   Expression entered was deemed invalid - please try again. *");
				Console.WriteLine("***************************************************************");
				Console.ReadKey();
				goto Start;
			}


		}

	}



}